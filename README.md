# Sistema de Troca de Mensagens Multiprotocolo utilizando AMQP e IRC


## P3 - Sistemas Distribuídos 2018/02

#### Membros:

- [Júlio Sousa](@juliosousa) *- Desenvolvedor*

- [Tiago Hermano](@tiagohermano) *- Líder/Desenvolvedor*

- [Vitor de Lima](@vitorlc) *- Documentação*

- [Denis Hideo Masunaga](@denismasunaga) *- Desenvolvedor*

- [Matheus de Assis](@matheus.dea) *- Documentação*

# Saiba Mais
**Acesse a documentação completa na [Wiki](https://gitlab.com/ad-si-2018-2/p3-g3/wikis/home).**